# Map Generator Plugin

This UE4 plugin creates a procedural landscape based on controllable areas created by a voronoi partition.
The height level in the respective areas is then controlled by perlin noise functions, which can be controlled by specifying sets of amplitudes and wavelengths.

![ExampleMap](Docu/Images/ExampleMap.png)

## How to create a Landscape

We drag and drop the landscape actor into the world. If we take the provided `BP_MapGenerator` (Derived from `MapGenerator`) the areas are directly colored.

![EmptyMap](Docu/Images/EmptyMap.png)

Now we can add some more areas to the initial three default areas by simply adding more elements to the `Area Input Data` array.
```
Warning: There is currently a bug, that the editor crashes when two areas have the same origin.
``` 

![MoreAreas](Docu/Images/MoreAreas.png)

Next we adjust the general noise parameters applied to the whole landscape. We add a wave parameter with a wave length and amplitude as we like and play around with the seed.
We also make the `AreaMeshes` component invisible, this is just a helper to visualize the areas as flat polygons. We can also give the `AreaGridLines`component another material.
These grid lines make use of the height level functions, so we still can visualize the area borders.

![BaselineNoise](Docu/Images/BaselineNoise.png)

The areas are the core model of this map generator. Let's add an element to the `AreaNoiseParameter` map and define some wave lengths and amplitudes for, e.g., mountains.
Mountains should be higher than the average map, so let's add some positive `Constant Offset`.

![ConstantOffsetMountain](Docu/Images/ConstantOffsetMountain.png)

This looks a little steep. We should now adjust the `AreaBorderNoiseFallOffDistance` (name is WIP), to regulate the influence of of the height noise towards the area border.
If we take 2000 as a value, that means that at 2000 unreal units away from the area border the noise is fully applied. At the border we multiply the noise with 0 and between
this factor is linearly interpolated. 

![NoiseFallOffDistance](Docu/Images/NoiseFallOffDistance.png)

We now have some visible grid lines. Our map resolution is propably not high enough for the amplitudes in the height function. The grid view might be handy.

![LowResolutionGrid](Docu/Images/LowResolutionGrid.png)

We can increase the grid factor for better height map sampling.

![HighResolutionGrid](Docu/Images/HighResolutionGrid.png)

Now we can see better, that our height function is still boring. Time for some perlin noise. Back to the `AreaNoiseParameter` map and add some wave parameters. 

![NoisyMountains](Docu/Images/NoisyMountains.png)

and thats for now the whole idea behind this little plugin. Playing with this is a lot of fun. Adding some more noise parameters to the other area types, setting the type for each area
and applying some landscape material...

![Finished](Docu/Images/Finished.png)


## Future plans

* Add algorithm to define random distribution of areas depending on parameters like number of players, areas per player, area distributions etc.
* Base mesh consists of quadliterals right now. Add option to switch to equiliteral triangles which can be aligned to a hexahedron tile mesh.
* Mesh partitioning to boost performance on large maps.
* Rivers
* Smooth area transitions
* Streets
* Baseline hight levels for each specific area
* special treatments to areas at landscape borders, e.g., no height fall off.
