#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SplineComponent.h"

#include "River.generated.h"

UCLASS()
class MAPGENERATORPLUGIN_API ARiver : public AActor
{
	GENERATED_BODY()

public:
	UPROPERTY()
	USplineComponent* SplineComponent;	
	
public:	
	ARiver();

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

};
