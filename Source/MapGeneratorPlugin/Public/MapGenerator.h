#pragma once

#include "CoreMinimal.h"

#include "Graph.h"
#include "GameFramework/Actor.h"
#include "NoiseGenerator.h"
#include "Polyhedron2D.h"
#include "River.h"
#include "Engine/TextRenderActor.h"

#include "MapGenerator.generated.h"

UENUM(BlueprintType)
enum class EAreaTypes : uint8
{
	Unset UMETA(DisplayName = "Unset"),
	Sea UMETA(DisplayName = "Sea"),
	Plain UMETA(DisplayName = "Plain"),
	Forest UMETA(DisplayName = "Forest"),
	Mountain UMETA(DisplayName = "Mountain")
};

USTRUCT(BlueprintType)
struct FAreaMetaData
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FVector2D Origin;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	EAreaTypes Type;
};

class FAreaData : public FGraphNode
{
public:
	EAreaTypes Type;

	FAreaData(FVector2D const& Location, EAreaTypes Type)
		: FGraphNode(Location), Type(Type) {}
};

USTRUCT(BlueprintType)
struct FRiverData
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (ClampMin = 0));
	int StartNode;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (ClampMin = 0));
	TArray<int> MiddleNodes;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (ClampMin = 0));
	int EndNode;

	TArray<int> GetAllIndicesInSingleArray() const
	{
		TArray<int> Indices{StartNode};
		Indices.Append(MiddleNodes);
		Indices.Add(EndNode);

		return Indices;
	}
};

UCLASS()
class MAPGENERATORPLUGIN_API AMapGenerator : public AActor
{
	GENERATED_BODY()

public:
	// Primary result of this actor. The landscape mesh as a composition input data.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "MapGenerator|Basic")
	class UProceduralMeshComponent* LandscapeMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MapGenerator|Basic")
	FIntPoint LandscapeResolution{50, 50};

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MapGenerator|Basic")
	FVector2D LandscapeMinCoords{0.f, 0.f};

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MapGenerator|Basic")
	FVector2D LandscapeMaxCoords{10000.f, 10000.f};

	// Helper mesh to visualize the areas
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "MapGenerator|Areas")
	class UProceduralMeshComponent* AreaMeshes;

	// Helper mesh to visualize the areas
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "MapGenerator|Areas")
	class UProceduralMeshComponent* AreaGridLines;

	// Specify origin position and type of the areas. Areas are then created as voronoi cells
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MapGenerator|Areas")
	TArray<FAreaMetaData> AreaInputData{
		FAreaMetaData{FVector2D(500.f, 600.f), EAreaTypes::Forest},
		FAreaMetaData{FVector2D(8000.f, 4000.f), EAreaTypes::Plain},
		FAreaMetaData{FVector2D(2500.f, 7000.f), EAreaTypes::Mountain}};
	
	// Specify data on edges between areas. Size of this array is set automatically.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MapGenerator|Areas")
	TArray<FRiverData> RiverInputData;

	// Helper mesh to visualize the areas
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "MapGenerator|Graph")
	class UProceduralMeshComponent* PrimeGraphMesh;

	// Helper mesh to visualize the areas
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "MapGenerator|Graph")
	class UProceduralMeshComponent* DualGraphMesh;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "MapGenerator|Graph")
	bool ShowPrimeGraphLabels;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "MapGenerator|Graph")
	bool ShowDualGraphLabels;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MapGenerator|Heightmap")
	int Seed = INT_MAX;

	// General noise parameters applied to the whole landscape
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MapGenerator|Heightmap")
	FNoiseParametersMultiWave GeneralNoiseParameters;

	// Height level in the areas is determined by multiple noise signals described with wave length and amplitude
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MapGenerator|Heightmap")
	TMap<EAreaTypes, FNoiseParametersMultiWave> AreaNoiseParameters;

	// Distance in where the noise is multiplied with a value between 0 (border) and 1 (at AreaBorderNoiseFallOffDistance)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MapGenerator|Heightmap")
	TMap<EAreaTypes, float> AreaBorderNoiseFallOffDistance;

private:
	FNoiseGenerator GeneralNoiseGenerator;
	TMap<EAreaTypes, FNoiseGenerator> AreaNoiseGenerators;

	FGraph PrimeGraph;
	FGraph DualGraph;

	UPROPERTY()
	TArray<ARiver*> Rivers;

	UPROPERTY()
	TArray<ATextRenderActor*> Labels;

public:
	AMapGenerator();

#if WITH_EDITOR
	virtual void PostEditChangeProperty(struct FPropertyChangedEvent& Event) override;
#endif

protected:
	virtual void PostActorCreated() override;
	virtual void PostLoad() override;
	
private:
	float GetHeight(FVector2D const& Location) const;

	std::function<float (FVector2D const&)> GetHeightFunction() const;

	FVector2D ClampLocationIntoMapBounds(FVector2D const& Location) const;

	// Returns the Area of the corresponding location. Location is first clamped into the map bounds.
	FAreaData* GetSurroundingArea(FVector2D const& Location) const;

	// Convert the input data into FAreaData, which describes the areas as voronoi cells / polyhedra.
	void CreateVoronoiAreas();

	// Fills the procedural mesh components with basic data from the areas. To visualize the polyhedral shape, draws borders.
	void CreateAreaMeshes();

	void SetupNoiseGenerators();

	void CreateAreaBorderLinesMesh();
	void UpdateAreaBorderLinesMesh();

	std::pair<TArray<FVector2D>, TArray<int>> GetAreaBorderLines() const;

	// Creates a cartesian landscape mesh, applies the height function and calculates normals.
	void CreateLandscapeMesh();
	void UpdateLandscapeMesh();

	TArray<FVector> GetLandscapeVertices() const;
	TArray<FVector> GetLandscapeNormals() const;
	TArray<int> GetLandscapeTriangles() const;

	void SanitizeRiverInputData();
	void UpdateRivers();
	void UpdateGraphLabels();
};

inline FVector2D AMapGenerator::ClampLocationIntoMapBounds(FVector2D const& Location) const
{
	return {FMath::Clamp(Location.X, LandscapeMinCoords.X, LandscapeMaxCoords.X), 
			FMath::Clamp(Location.Y, LandscapeMinCoords.Y, LandscapeMaxCoords.Y)};
}

inline FAreaData* AMapGenerator::GetSurroundingArea(FVector2D const& Location) const
{
	FVector2D ValidLocation = ClampLocationIntoMapBounds(Location);

	for (auto Area : PrimeGraph.GetNodes())
		if (Area->GetVoronoiCell().IsPointInPolyhedron(ValidLocation))
			return (FAreaData*) Area.Get();

	throw std::runtime_error("Location could not be associated with Area");
}