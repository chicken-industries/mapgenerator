#include "River.h"

ARiver::ARiver()
{
	PrimaryActorTick.bCanEverTick = true;

	SplineComponent = CreateDefaultSubobject<USplineComponent>(TEXT("Spline"));
	RootComponent = SplineComponent;
}

void ARiver::BeginPlay()
{
	Super::BeginPlay();
	
}

void ARiver::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

