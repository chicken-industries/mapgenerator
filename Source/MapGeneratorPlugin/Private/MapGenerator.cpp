#include "MapGenerator.h"

#include "ArrayHelpers.h"
#include "GridMath.h"
#include "Mappings.h"
#include "Kismet/KismetSystemLibrary.h"
#include "ProceduralMeshComponent.h"
#include "Components/TextRenderComponent.h"

AMapGenerator::AMapGenerator()
{
	AreaMeshes = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("AreaMeshes"));
	RootComponent = AreaMeshes;

	AreaGridLines = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("AreaGridLines"));
	AreaGridLines->SetupAttachment(RootComponent);
	AreaGridLines->SetRelativeLocation(FVector(0.f, 0.f, 1.f));

	LandscapeMesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("LandscapeMesh"));
	LandscapeMesh->SetupAttachment(RootComponent);

	PrimeGraphMesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("PrimeGraphMesh"));
	PrimeGraphMesh->SetupAttachment(RootComponent);

	DualGraphMesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("DualGraphMesh"));
	DualGraphMesh->SetupAttachment(RootComponent);
}

void AMapGenerator::PostActorCreated()
{
	Super::PostActorCreated();

	CreateVoronoiAreas();
	SetupNoiseGenerators();

	CreateAreaMeshes();
	CreateAreaBorderLinesMesh();
	CreateLandscapeMesh();
}

void AMapGenerator::PostLoad()
{
	Super::PostLoad();

	CreateVoronoiAreas();
	SetupNoiseGenerators();
}

#if WITH_EDITOR
void AMapGenerator::PostEditChangeProperty(struct FPropertyChangedEvent& Event)
{
	Super::PostEditChangeProperty(Event);

	FName MemberPropertyName = Event.MemberProperty ? Event.MemberProperty->GetFName() : NAME_None;

	if (MemberPropertyName == GET_MEMBER_NAME_CHECKED(AMapGenerator, AreaInputData) ||
		MemberPropertyName == GET_MEMBER_NAME_CHECKED(AMapGenerator, LandscapeMinCoords) ||
		MemberPropertyName == GET_MEMBER_NAME_CHECKED(AMapGenerator, LandscapeMaxCoords))
	{
		CreateVoronoiAreas();
		UpdateRivers();

		CreateAreaMeshes();
		CreateAreaBorderLinesMesh();
		UpdateLandscapeMesh();

		UpdateGraphLabels();
	}
	else if (MemberPropertyName == GET_MEMBER_NAME_CHECKED(AMapGenerator, LandscapeResolution))
	{
		CreateLandscapeMesh();
	}
	else if (MemberPropertyName == GET_MEMBER_NAME_CHECKED(AMapGenerator, AreaNoiseParameters) ||
			 MemberPropertyName == GET_MEMBER_NAME_CHECKED(AMapGenerator, AreaBorderNoiseFallOffDistance) ||
			 MemberPropertyName == GET_MEMBER_NAME_CHECKED(AMapGenerator, GeneralNoiseParameters) ||
			 MemberPropertyName == GET_MEMBER_NAME_CHECKED(AMapGenerator, Seed))
	{
		SetupNoiseGenerators();
		UpdateLandscapeMesh();
		UpdateAreaBorderLinesMesh();
	}
	else if (MemberPropertyName == GET_MEMBER_NAME_CHECKED(AMapGenerator, RiverInputData))
	{
		UpdateRivers();
	}
	else if (MemberPropertyName == GET_MEMBER_NAME_CHECKED(AMapGenerator, ShowPrimeGraphLabels) ||
			 MemberPropertyName == GET_MEMBER_NAME_CHECKED(AMapGenerator, ShowDualGraphLabels))
	{
		UpdateGraphLabels();
	}
}
#endif

void AMapGenerator::CreateVoronoiAreas()
{
	PrimeGraph = FGraph(LandscapeMinCoords, LandscapeMaxCoords);

	for (auto const& Area : AreaInputData)
		PrimeGraph.AddNode(FAreaData(Area.Origin, Area.Type));

	PrimeGraph.CreateEdgesByDelaunyTriangulation<FGraphEdge>();

	DualGraph = PrimeGraph.ComputeDualGraph<FGraphNode, FGraphEdge>();
}

void AMapGenerator::CreateAreaMeshes()
{
	AreaMeshes->ClearAllMeshSections();

	int AreaID = 0;

	for (auto const& Area : PrimeGraph.GetNodes())
	{
		TArray<FVector2D> Vertices;
		TArray<int> TriangleIDs;
		Area->GetVoronoiCell().ComputeTriangulation(Vertices, TriangleIDs);

		AreaMeshes->CreateMeshSection_LinearColor(AreaID++, ArrayHelpers::ConvertVectorArray(Vertices), TriangleIDs, {},
			{}, {}, {}, true);
	}

	auto PrimeGraphData = PrimeGraph.VisualizeAsProceduralMeshComp();
	
	PrimeGraphMesh->ClearAllMeshSections();
	PrimeGraphMesh->CreateMeshSection_LinearColor(0, ArrayHelpers::ConvertVectorArray(PrimeGraphData.first), PrimeGraphData.second, {},
			{}, {}, {}, false);
	
	auto DualGraphData = DualGraph.VisualizeAsProceduralMeshComp();
	
	DualGraphMesh->ClearAllMeshSections();
	DualGraphMesh->CreateMeshSection_LinearColor(0, ArrayHelpers::ConvertVectorArray(DualGraphData.first), DualGraphData.second, {},
			{}, {}, {}, false);
}

void AMapGenerator::SetupNoiseGenerators()
{
	GeneralNoiseGenerator.SetSeed(Seed);
	GeneralNoiseGenerator.SetNoiseParameters(GeneralNoiseParameters);

	AreaNoiseGenerators.Empty();

	for (auto const& Input : AreaNoiseParameters)
		AreaNoiseGenerators.Add(Input.Key, FNoiseGenerator(Input.Value, Seed));
}

void AMapGenerator::CreateAreaBorderLinesMesh()
{
	auto VerticesAndTriangles = GetAreaBorderLines();

	AreaGridLines->CreateMeshSection_LinearColor(0, ArrayHelpers::ConvertVectorArray(VerticesAndTriangles.first, GetHeightFunction()),
		VerticesAndTriangles.second, {}, {}, {}, {}, false);
}

void AMapGenerator::UpdateAreaBorderLinesMesh()
{
	auto VerticesAndTriangles = GetAreaBorderLines();
	
	AreaGridLines->UpdateMeshSection_LinearColor(0, ArrayHelpers::ConvertVectorArray(VerticesAndTriangles.first, GetHeightFunction()),
		{}, {}, {}, {});
}

std::pair<TArray<FVector2D>, TArray<int>> AMapGenerator::GetAreaBorderLines() const
{
	TArray<FVector2D> GridLineVertices;
	TArray<int> GridLineTriangleIDs;
	
	for (auto const& Area : PrimeGraph.GetNodes())
		Area->GetVoronoiCell().ComputeTriangulatedBorders(GridLineVertices, GridLineTriangleIDs, true, 0.01f, 5);

	return {GridLineVertices, GridLineTriangleIDs};
}

void AMapGenerator::CreateLandscapeMesh()
{
	LandscapeMesh->CreateMeshSection_LinearColor(
		0, GetLandscapeVertices(), GetLandscapeTriangles(), GetLandscapeNormals(), {}, {}, {}, true);
}

void AMapGenerator::UpdateLandscapeMesh()
{
	LandscapeMesh->UpdateMeshSection_LinearColor(0, GetLandscapeVertices(), GetLandscapeNormals(), {}, {}, {});
}

TArray<FVector> AMapGenerator::GetLandscapeVertices() const
{
	TArray<FVector2D> Vertices = GridMath::CreateEquidistantCartesianGridVertices(LandscapeMinCoords, LandscapeMaxCoords, LandscapeResolution);
	
	return ArrayHelpers::ConvertVectorArray(Vertices, GetHeightFunction());
}

TArray<FVector> AMapGenerator::GetLandscapeNormals() const
{
	TArray<FVector2D> Vertices = GridMath::CreateEquidistantCartesianGridVertices(LandscapeMinCoords, LandscapeMaxCoords, LandscapeResolution);
	
	return GridMath::CalcNormalOnSurfaceByHeightMap(Vertices, GetHeightFunction());
}

TArray<int> AMapGenerator::GetLandscapeTriangles() const
{
	return GridMath::CreateCartesianGridTriangles(LandscapeResolution);
}

void AMapGenerator::SanitizeRiverInputData()
{
	for (auto& Input : RiverInputData)
	{
		Input.StartNode = FMath::Clamp(Input.StartNode, 0, DualGraph.GetNodes().Num() - 1);

		for (auto& MiddleNode : Input.MiddleNodes)
			MiddleNode = FMath::Clamp(MiddleNode, 0, DualGraph.GetNodes().Num() - 1);
		
		Input.EndNode = FMath::Clamp(Input.EndNode, 0, DualGraph.GetNodes().Num() - 1);
	}
}

void AMapGenerator::UpdateRivers()
{
	SanitizeRiverInputData();
	
	for (auto const& River : Rivers)
		River->Destroy();
	
	Rivers.Empty();

	for (auto const& Input : RiverInputData)
	{
		auto NewRiver = GetWorld()->SpawnActor<ARiver>();

		Rivers.Add(NewRiver);

		auto Location = DualGraph.GetNodeLocations()[Input.StartNode];
		NewRiver->SetActorLocation(FVector(Location, 0));

		auto DualNodes = DualGraph.GetNodes();
		
		TArray<int> RiverKeyNodeIndices = Input.GetAllIndicesInSingleArray();
		
		for (int i = 0; i < RiverKeyNodeIndices.Num() - 1; ++i)
		{
			bool PathFound; // TODO: nodes might be not connected
			auto Path = DualGraph.ShortestPath(DualNodes[RiverKeyNodeIndices[i]], DualNodes[RiverKeyNodeIndices[i+1]], PathFound);

			for (auto const& Node : Path)
				NewRiver->SplineComponent->AddSplinePoint(FVector(Node->GetLocation(), 0), ESplineCoordinateSpace::World);
		}
	}
}

void AMapGenerator::UpdateGraphLabels()
{
	for (auto const& Label : Labels)
		Label->Destroy();

	Labels.Empty();

	int NodeID = 0;

	if (ShowPrimeGraphLabels)
	{
		for (auto const& Location : PrimeGraph.GetNodeLocations())
		{
			auto NewTextRender = GetWorld()->SpawnActor<ATextRenderActor>();
			Labels.Add(NewTextRender);

			FString TheText = FString::Format(TEXT("Node {0}"), {NodeID++});
			NewTextRender->GetTextRender()->SetText(FText::FromString(TheText));
			
			NewTextRender->GetTextRender()->SetTextRenderColor(FColor(0, 0, 255));

			NewTextRender->SetActorLocation(FVector(Location, 10.f));
		}
		NodeID = 0;
		
		for (auto const& Edge : PrimeGraph.GetEdges())
		{
			auto NewTextRender = GetWorld()->SpawnActor<ATextRenderActor>();
			Labels.Add(NewTextRender);

			FString TheText = FString::Format(TEXT("Edge {0}"), {NodeID++});
			NewTextRender->GetTextRender()->SetText(FText::FromString(TheText));
			
			NewTextRender->GetTextRender()->SetTextRenderColor(FColor(0, 0, 255));
			
			NewTextRender->SetActorLocation(FVector(Edge->GetAverageNodeLocation(), 10.f));
		}
		NodeID = 0;
	}

	if (ShowDualGraphLabels)
	{
		for (auto const& Location : DualGraph.GetNodeLocations())
		{
			auto NewTextRender = GetWorld()->SpawnActor<ATextRenderActor>();
			Labels.Add(NewTextRender);

			FString TheText = FString::Format(TEXT("Node {0}"), {NodeID++});
			NewTextRender->GetTextRender()->SetText(FText::FromString(TheText));
			
			NewTextRender->GetTextRender()->SetTextRenderColor(FColor(255, 0, 0));

			NewTextRender->SetActorLocation(FVector(Location, 10.f));
		}
		NodeID = 0;
	
		for (auto const& Edge : DualGraph.GetEdges())
		{
			auto NewTextRender = GetWorld()->SpawnActor<ATextRenderActor>();
			Labels.Add(NewTextRender);

			FString TheText = FString::Format(TEXT("Edge {0}"), {NodeID++});
			NewTextRender->GetTextRender()->SetText(FText::FromString(TheText));
			
			NewTextRender->GetTextRender()->SetTextRenderColor(FColor(255, 0, 0));
			
			NewTextRender->SetActorLocation(FVector(Edge->GetAverageNodeLocation(), 10.f));
		}
	}

	for (auto& Label : Labels)
	{
		Label->GetTextRender()->SetHorizontalAlignment(EHTA_Center);
		Label->GetTextRender()->SetVerticalAlignment(EVRTA_TextCenter);
		Label->GetTextRender()->SetWorldSize(300.f);
		
		Label->SetActorRotation(FRotator(90.f, 90.f, -90.f));
	}
}

float AMapGenerator::GetHeight(FVector2D const& Location) const
{
	float Height = GeneralNoiseGenerator.PerlinNoise(Location);

	FAreaData const* SurroundingArea = GetSurroundingArea(Location);

	if (AreaNoiseParameters.Contains(SurroundingArea->Type))
	{
		float DistanceToPolyBorder = SurroundingArea->GetVoronoiCell().GetDistanceToPolyBorder(Location);

		float FallOffDistance = AreaBorderNoiseFallOffDistance.Contains(SurroundingArea->Type)
									? AreaBorderNoiseFallOffDistance[SurroundingArea->Type]
									: 1.f;

		float SmoothingFactor = UMappings::SmoothStep3(FMath::Clamp(DistanceToPolyBorder / FallOffDistance, 0.f, 1.f));

		Height += SmoothingFactor * AreaNoiseGenerators[SurroundingArea->Type].PerlinNoise(Location);
	}

	return Height;
}

std::function<float(FVector2D const&)> AMapGenerator::GetHeightFunction() const
{
	AMapGenerator const* MapGen = this;
	return [MapGen] (FVector2D const& Location)
	{
		return MapGen->GetHeight(Location);
	};
}
